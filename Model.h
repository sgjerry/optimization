/********************************************************
  ::::::::::::::::::::::::::::::::::::::::::::::::::::
  
 
  @File:   Model.h 
  @Author: Jerry Shi
  
  @Created: March 8, 2015 
    
  :::::::::::::::::::::::::::::::::::::::::::::::::::
 *******************************************************/


#ifndef MODEL_H
#define	MODEL_H

class Model{
  public:
    virtual Model() = 0;
    Model(const Model& orig);
    ~Model;
};

#endif	/* MODEL_H */

/********************************************************
  ::::::::::::::::::::::::::::::::::::::::::::::::::::
  
  This file defines a template for performing genetic 
  algorithm. In order to utilize this template, users
  will have to define their own model file. 
 
  @File:   GeneAlgoFrame.h 
  @Author: Jerry Shi
  
  @Created: March 4, 2013
  @Updated: March 8, 2015 
    
  :::::::::::::::::::::::::::::::::::::::::::::::::::
 *******************************************************/

#include "Random.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <sstream>
#include <cstdlib>
#include <ctime>

#ifndef GAFRAME_H
#define	GAFRAME_H

enum SelectionType{
    TOURNAMENTSELECTION = 0
};

enum RandomSeedType{
    RANDOMPICK = 0,  // randomly pick using current time
    GIVENBYUSER
};

// Using integer type as MutationType and CrossoverType
// is due to the reason that user could define the type themselves
// inside the model class.
// These two types will be pass back to the model class and be interpreted.
typedef long MutationType;
typedef long CrossoverType;

template<typename ModelType>
class GeneAlgoFrame {

  public:

    /**
     * Constructor for initializing the template for 
     * genetic algorithm, from input file.
     * @param infile: inputfile name for parameters
     */
    GeneAlgoFrame(std::string infile);

    /**
     * Copy Constructor
     * @param orig: a GeneAlgoFrame object
     */
    GeneAlgoFrame(const GeneAlgoFrame& orig);

    /**
     * Destructor
     */
    ~GeneAlgoFrame();
    
    /**
     * This function should be called before starting
     * the (re)running of GA. 
     * @param s: random seed, default value is negative, which means
     * RANDOMPICK type.
     */
    void init(long s = -1);

    /**
     * Find the best individual in given population
     * @param pop: 2D array that store the population
     * @param size: size of the population
     * @return: return the index of the best individual
     */
    long findBest(const ModelType** pop, size) const;
    
    /**
     * This function tests the stopping criterion.
     * @return: a boolean value, true if the stopping criterion
     * has been satisfied; otherwise false.
     */
    inline bool isComplete();

    /**
     * Run one cycle of GA, including: elitism(if chosen), selection, crossover and mutation.
     */
    void evolve();

    /**
     * Start running GA, with given random seed
     * @param s: random seed, default value is negative, which means
     * RANDOMPICK type.
     */
    void run(long s = -1);
    
    /**
     * Select
     * @return: return the index selected from mPopulation
     */
    long select();
    
    /**
     * Print out the parameter of this GA frame
     * @param fout: output stream, default is cout
     */
    void print(std::ostream& fout=std::cout);
    
    
  private:
    
    /**
     * Reading parameters from given input file.
     * This is a private function, and will be called by the constructor.
     * @param infile: input filename
     */
    void input(std::string infile);


    /**
     * Clearn up the memory acquired by pointers
     */
    void cleanup();
    
    bool mElitismFlag;              // indicator of elitism
    
    long mPopulationSize;           // population size        
    long mMaxGenNum;                // number of max generations
    long mStableGenThreshold;       // threshold of stable generations
    long mStableGenCount;           // counter of stable generations
    long mCurrentGenCount;          // counter of current generation
    long mRandomSeed;               // random number seed
    long mTournamentSize;           // tournament size

    double mProbOfCrossover;        // probability of crossover
    double mProbOfMutation;         // probability of mutation
    
    ModelType** mPopulation;        // population
    ModelType** mNewPopulation;     // new population
    ModelType* mBestIndiv;          // globally best individual 
    Random* mRandom;                // random number generator
    
    SelectionType mSelectionType;   // selection type
    CrossoverType mCrossoverType;   // crossover type
    MutationType mMutationType;     // mutation type
    RandomSeedType mRandomSeedType; // random number selection type

};

template<typename ModelType>
GeneAlgoFrame<ModelType>::GeneAlgoFrame(string infile){

    input(infile);

    mElitismFlag          = false;
    mBestIndiv            = NULL;
    mRandom               = NULL;
    mPopulation           = NULL;
    mNewPopulation        = NULL;
    mBestIndiv            = NULL;
    mTournamentSize       = 4;
    mRandomSeedType       = RANDOMPICK;

    mPopulation           = new ModelType*[mPopulationSize];
    mNewPopulation        = new ModelType*[mPopulationSize];
    mBestIndiv            = new ModelType();
    for(int i=0; i<mPopulationSize; ++i){
        mPopulation[i]    = new ModelType();
        mNewPopulation[i] = new ModelType();
    }

}

template<typename ModelType>
GeneAlgoFrame<ModelType >::GeneAlgoFrame(const GeneAlgoFrame& orig){
    mElitismFlag          = orig.mElitismFlag;
    mPopulationSize       = orig.mPopulationSize;
    mMaxGenNum            = orig.mMaxGenNum;
    mStableGenThreshold   = orig.mStableGenThreshold;
    mStableCount          = orig.mStableCount;
    mCurrentGenCount      = orig.mCurrentGenCount;
    mRandomSeed           = orig.mRandomSeed;
    mTournamentSize       = orig.mTournamentSize;
    mProbOfCrossover      = orig.mProbOfCrossover;
    mProbOfMutation       = orig.mProbOfMutation;
    mSelectionType        = orig.mSelectionType;
    mCrossoverType        = orig.mCrossoverType;
    mMutationType         = orig.mMutationType;
    mRandomSeedType       = orig.mRandomSeedType;

    cleanup();

    mRandom               = orig.mRandom;
    mBestIndiv            = orig.mBestIndiv;
    mPopulation           = new ModelType*[mPopulationSize];
    mNewPopulation        = new ModelType*[mPopulationSize];
    for(int i=0; i<mPopulationSize; ++i){
        mPopulation[i]    = orig.mPopulation[i];
        mNewPopulation[i] = orig.mNewPopulation[i];
    }
}

template<typename ModelType>
GeneAlgoFrame<ModelType>::~GeneAlgoFrame(){
    cleanup();
}

template<typename ModelType>
GeneAlgoFrame<ModelType>::cleanup(){
    for(int i=0; i < mPopulationSize; ++i){
        if(mPopulation[i]    != NULL) delete mPopulation[i];
        if(mNewPopulation[i] != NULL) delete mNewPopulation[i];
    }
    if(mNewPopulation != NULL) delete [] mNewPopulation;
    if(mPopulation    != NULL) delete [] mPopulation;
    if(mBestIndiv     != NULL) delete mBestIndiv;
    if(mRandom        != NULL) delete mRandom;
}

template<typename ModelType>
void GeneAlgoFrame<ModelType >::init(long s){
    
    mStableGenCount = 0;
    mCurrentGenCount = 0;
    
    // decide how to assign the random seed
    // default way is randomly pick using current time
    switch(mRandomSeedType){
        case RANDOMPICK:
            mRandomSeed = time(NULL);
            break;
        case GIVENBYUSER:
            s < 0: mRandomSeed = time(NULL) : mRandomSeed = s;
            break;
    }
    if(mRandom != NULL) delete mRandom;
    mRandom -> new Random(mRandomSeed);
    
    // initialize each individuals, calculate their fitness
    for(int i = 0; i < mPopulationSize; ++i){
        mPopulation[i]  ->  init();
        mPopulation[i]  ->  evaluate();
    }

    // record the best individual
    long bIndex = findBest(mPopulation, mPopulationSize);
    *mBestIndiv = *mPopulation[bIndex];
}

template<typename ModelType>
long GeneAlgoFrame<ModelType>::findBest(ModelType** pop, long size){
    long bIndex = 0;
    for(int i=1; i < size; ++i){
        if(pop[i]-> compare(pop[bIndex])){
            bIndex = i;
        }
    }
    return bIndex;
}

template<typename ModelType>
void GeneAlgoFrame<ModelType>::print(std::ostream& fout){
    fout   << "Population Size:    "            << mPopulationSize      << "\n"
           << "Max. Gen. Num.:    "             << mMaxGenNum           << "\n"
           << "Stable. Gen. Threshold:    "     << mStableGenThreshold  << "\n"
           << "Elitism Flag:    "               << mElitismFlag         << "\n"
           << "Tournament Size:    "            << mTournamentSize      << "\n"
           << "Crossover Rate:    "             << mProbOfCrossover     << "\n"
           << "Mutation Rate:    "              << mProbOfMutation      << "\n"
           << "Crossover Type:    "             << mCrossoverType       << "\n"
           << "Mutation Type:    "              << mMutationType        << "\n" 
           << "Selection Type:    "             << mSelectionType       << std::endl;
}

template<typename ModelType>
void GeneAlgoFrame<ModelType>::input(string infile){

    std::string modelfile;

    std::ifstream fin(infile.c_str());
    if(!fin.is_open()){
        std::cout << "Error: cannot open file-> " << infile << endl;
        exit(1);
    }
    std::string line, word;
    while(getline(fin,line)){
        if(line[0] == '/' || line[0] == '#') continue;
        std::istringstream sin(line);
        std::string word;
        sin >> word;
        if(word.compare("POPULATIONSIZE")==0){
            sin >> mPopulationSize;
        }else if(word.compare("MAXGENERATION")==0){
            sin >> mMaxGenNum;
        }else if(word.compare("STABLEGENERATION")==0){
            sin >> mStableGenThreshold;
        }else if(word.compare("ELITISM")==0){
            long e; sin >> e;
            e == 0 ? mElitismFlag = false : mElitismFlag = true;
        }else if(word.compare("TOURNAMENTSIZE")==0){
            sin >> mTournamentSize;
        }else if(word.compare("CROSSOVERRATE")==0){
            sin >> mProbOfCrossover;
        }else if(word.compare("MUTATIONRATE")==0){
            sin >> mProbOfMutation;
        }else if(word.compare("CROSSOVERTYPE")==0){
            sin >> mCrossoverType;
        }else if(word.compare("MUTATIONTYPE")==0){
            sin >> mMutationType;
        }else if(word.compare("SELECTIONTYPE")==0){
            sin >> word;
            if(word.compare("TOURNAMENTSELECTION") == 0){
                mSelectionType = TOURNAMENTSELECTION;
            }else{
                std::cout << "Illegal SELECTIONTYPE!" << endl;
                exit(1);
            }
        }else if(word.compare("RANDOMSEEDTYPE")==0){
            sin >> word;
            if(word.compare("RANDOMPICK") == 0){
                mRandomSeedType = RANDOMPICK;
            }else if(word.compare("GIVENBYUSER") == 0){
                mRandomSeedType = GIVENBYUSER;
            }else{
                std::cout << "Illegal RANDOMSEEDTYPE!" << endl;
                exit(1);
            }
        }else if(word.compare("MODELFILE")==0){
            sin >> modelfile;
        }
    }
    fin.close();
    if(!modelfile.empty()) ModelType::Input(modelfile);

}

template<typename ModelType >
bool GeneAlgoFrame<ModelType >::isComplete(){
    // stopping criterion: current genration larger than the
    // maximum generation or stable generation larger than the 
    // threshold.
    return (mCurrentGenCount >= mMaxGenNum || mStableGenCount >= mStableGenThreshold);
}

template<typename ModelType >
void GeneAlgoFrame<ModelType >::run(long s){
    init(s);
    while(!isComplete()){
        // if the GA has not meet the stopping criterion,
        // keep evolving. 
        evolve();
    }
}

template<typename ModelType >
void GeneAlgoFrame<ModelType >::evolve(){
    mCurrentGenCount++;
    
    long index(0);
    long pi[2], ci[2]; // parent indexes and children indexes
    
    // Elitism: pick the best individual and 
    // make two copied of it into the new generation
    if(mElitismFlag){
        long elitismIndex(0);
        for(long i = 1; i < mPopulationSize; ++i){
            if(mPopulation[i] -> compare(mPopulation[elitismIndex])){
               elitismIndex = i;
            }
        }
        *mNewPopulation[index] = mPopulation[elitismIndex];
        mNewPopulation[index++] -> evaluate();
        *mNewPopulation[index] = mPopulation[elitismIndex];
        mNewPopulation[index++] -> evaluate();
    }
    
    while(index < mPopulationSize){
        // :::::::::::::::::::::::::::::::::
        // Selection
        pi[0] = select(); pi[1] = select();
        ci[0] = index; ci[1] = index + 1;
        *mNewPopulation[c[0]] = *mPopulation[pi[0]];
        *mNewPopulation[c[1]] = *mPopulation[pi[1]];
        
        // :::::::::::::::::::::::::::::::::
        // Crossover
        if(mRandom -> nextDouble() < mProbOfCrossover) ModelType::Crossover(mCrossoverType, mNewPopulation[c[0]], mNewPopulation[c[1]]);
        
        // :::::::::::::::::::::::::::::::::
        // Mutation
        if(mRandom -> nextDouble() < mProbOfMutation) ModelType::Mutate(mMutationType, mNewPopulation[c[0]]);
        if(mRandom -> nextDouble() < mProbOfMutation) ModelType::Mutate(mMutationType, mNewPopulation[c[1]]);
        
        // ::::::::::::::::::::::::::::::::::
        // Evaluate New Chromosomes
        mNewPopulation[c[0]]->evaluate();
        mNewPopulation[c[1]]->evaluate();
        index += 2;
    }
    
    // ::::::::::::::::::::::::::::::::::
    // check the improvement in best guy
    long bIndex = findBest(mNewPopulation, mPopulationSize);
    if(mPopulation[bIndex] -> compare(mBestIndiv)){
        mStableGenCount = 0;
        *mBestIndiv = *mNewPopulation[bIndex];
    }else{
        mStableGenCount++;
    }
    
    // ::::::::::::::::::::::::::::::::::
    // deal with memory issues
    ModelType** a = pop;
    pop = mNewPopulation;
    mNewPopulation = a;
    a = NULL;
}

template<typename ModelType >
int GeneAlgoFrame<ModelType >::select(){
    switch(mSelectionType){
        case TOURNAMENTSELECTION:
            // tournament selection: 
            // randomly pick mTournamentSize individuals, among those
            // pick the best
            long* indexes = new long[mTournamentSize];
            long i = 0;
            while(i < mTournamentSize){
                indexes[i] = mRandom -> nextLong(mPopulationSize);
                for(int j=0; j<i; ++j){
                    if(indexes[j] == indexes[i]){
                        i--;
                        break;
                    }
                }
                i++;
            }
               
            long fi(indexes[0]);
            for(long k = 1; k < mTournamentSize; ++k){
                if( mPopulation[indexes[k]] -> compare(mPopulation[fi]) ){
                    fi = indexes[k];
                }
            }
            delete indexes;
            return fi;
            break;

        default:
            std::cout << "Illegal SELECTION-TYPE!" << endl;
            exit(1);
    }
}

#endif	/* GAFRAME_H */

